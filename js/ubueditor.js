
BUE.postprocess.ubumultiple = function(E, $) {
    E.showMultiDialog = function () {

    }

        for (var i = 0; i < E.tpl.buttons.length; i++) {
            if (E.tpl.buttons[i][1] == 'js: E.showMultiDialog();') {

                var $button = $('#bue-' + E.index + '-button-' + i);
                $button.prepend(' ');
                var $wrapper = $('<span class="uploadbox"><form id="file-form" action="/ubumultiple/upload" method="POST"> </form></span>');
                $button.wrap($wrapper);
                $( "#file-form" ).append("<input type='file' name='images' id='images' multiple='multiple' />");
                $( "#file-form" ).append( "<button type='submit' id='btn'>Upload Files!</button>" );
                $( ".uploadbox" ).after( "<div id='response'></div>");
                $( ".uploadbox" ).after( "<ul id='image-list'></ul>");
                $button.load(function() {
                    var buttonWidth = $button.width();
                    var buttonHeight = $button.height();
                });

//                   $( "#fileToUpload" ).change(function() {
                       var input = document.getElementById("images"),
                           formdata = false;

                       function showUploadedItem (source) {
                           var list = document.getElementById("image-list"),
                               li   = document.createElement("li"),
                               img  = document.createElement("img");
                           img.src = source;
                           li.appendChild(img);
                           list.appendChild(li);
                       }

                       if (window.FormData) {
                           formdata = new FormData();
                           document.getElementById("btn").style.display = "none";
                       }

                       input.addEventListener("change", function (evt) {
                           document.getElementById("response").innerHTML = "Uploading . . ."
                           var i = 0, len = this.files.length, img, reader, file;

                           for ( i=0 ; i < len; i++ ) {
                               file = this.files[i];

                               if (!!file.type.match(/image.*/)) {
                                   if ( window.FileReader ) {
                                       reader = new FileReader();
                                       reader.onloadend = function (e) {
                                           showUploadedItem(e.target.result, file.fileName);
                                       };
                                       reader.readAsDataURL(file);
                                   }
                                   if (formdata) {
                                       formdata.append("images[]", file);
                                   }
                               }
                           }

                           if (formdata) {
                               $.ajax({
                                   url: "/ubumultiple/upload",
                                   type: "POST",
                                   data: formdata,
                                   processData: false,
                                   contentType: false,
                                   success: function (res) {
                                       document.getElementById("response").innerHTML = res;
                                   }
                               });
                           }
                       }, false);
//                        $('#file-form').submit();
//                        var result = $(this).attr("files");
//                        var names = $.map(result, function(val) { return val.name; });
//                        $data = 'hello world';
//                        $.ajax({
//                            'url': '/ubumultiple/upload',
//                            'type': 'POST',
//                            'dataType': 'json',
//                            'data': $data,
//                            'success': function(data)
//                            {
//                                if(data == "success")
//                                {
//                                    alert('Submit ok');
//                                }
//                                else
//                                {
//                                   alert('Upload files problem, maybe you do something else?!');
//                                }
//                            }
//                        });
//                   });
            }

        }
    }


